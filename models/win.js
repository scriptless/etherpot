import babel from 'babel-register';
import mongoose from 'mongoose';

var Schema = mongoose.Schema;

// Win Schema
var winSchema = new Schema({
  address: {type: String, required: true},
  amount: {type: String, required: true},
  timestamp: {type: String, required: true},
});

var Win = module.exports = mongoose.model('win', winSchema);
