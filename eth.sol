pragma solidity ^0.4.4;

/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {
  address public owner;

  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  function Ownable() {
    owner = msg.sender;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    if (msg.sender != owner) {
      throw;
    }
    _;
  }

  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) onlyOwner {
    if (newOwner != address(0)) {
      owner = newOwner;
    }
  }

  /**
   * @dev Kills the contract and refunds it to owner addr.
   */
  function killContract() onlyOwner {
    selfdestruct(owner);
  }

}

/**
 * Math operations with safety checks
 */
library SafeMath {

  function mul(uint a, uint b) internal returns (uint) {
    uint c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint a, uint b) internal returns (uint) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint a, uint b) internal returns (uint) {
    assert(b <= a);
    return a - b;
  }

  function add(uint a, uint b) internal returns (uint) {
    uint c = a + b;
    assert(c >= a);
    return c;
  }

  function assert(bool assertion) internal {
    if (!assertion) {
      throw;
    }
  }
}

contract EthPot is Ownable {

    using SafeMath for uint;

    uint public amountPot;
    uint public deadline;
    uint public started;
    address owner;

    event Deposit(address _addr, uint _amount);
    event Finish(bool _succesful);

    struct Participant {
        address addr;
        uint amount;
        uint time;
    }

    mapping(address => Participant) public participants; //Stores Participants with addresses
    address[] public partiAccts; //Stores all addresses

    /**
     * Constructor function
     *
     * Setup the owner
     */
    function EthPot() public {
        started = 0;
        deadline = 0;
        owner = msg.sender;
    }

    /**
     * Fallback function
     *
     * The function without name is the default function that is called whenever anyone sends funds to a contract
     */
    function () payable public {
        require(deadline == 0 || now < deadline);

        uint _amount = msg.value;
        address _addr = msg.sender;

        require(_amount >= (1 ether/100));

        if(deadline == 0) {
          deadline = now + 2 minutes;
          started = now;
        }

        var participant = participants[_addr];

        // Check if address not contributed
        if(participant.addr == address(0)) {
          participant.addr = _addr;
          participant.amount += _amount;
          participant.time = now;
          partiAccts.push(_addr) -1;
        } else {
          participant.amount += _amount;
        }

        amountPot += _amount;

        Deposit(_addr, _amount);
    }

    function getParticipants() view public returns (address[]) {
        return partiAccts;
    }

    function getParticipant(address _addr) view public returns(uint, uint) {
        return (participants[_addr].amount, participants[_addr].time);
    }

    function numParti() view public returns (uint) {
        return partiAccts.length;
    }

    modifier afterDeadline() { if (passedDeadline()) _; }

    function passedDeadline() view public returns (bool) {
        return (now >= deadline && deadline != 0);
    }

    /**
     * After deadline functions
     *
     * Only applicable if now >= deadline
     */
    function checkIfPlayable() view public returns (bool) {
        return (numParti() >= 2 && passedDeadline());
    }

    function endPool(address winner) afterDeadline onlyOwner public {
        if (!checkIfPlayable()) {
            Participant storage p1 = participants[partiAccts[0]];
            uint amount = p1.amount;
            p1.amount = 0;
            sendEther(p1.addr, amount.mul(95).div(100));
            Finish(false);
        } else {
            sendEther(winner, amountPot.mul(95).div(100)); // 5% fees
            Finish(true);
        }
        killContract();
    }

    function sendEther(address addr, uint amount) onlyOwner public {
      if (amount > 0) addr.transfer(amount);
    }

}
