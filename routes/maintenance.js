import babel from 'babel-register';
import express from 'express';

var router = express.Router();

router.get('/', function (req, res) {
  res.render('maintenance', {layout: false});
});

module.exports = router;
