import babel from 'babel-register';
import express from 'express';
import Win from '../models/win';

var router = express.Router();

// Get Homepage
router.get('/', function(req, res) {
  res.render('index'

  , {
  // //   address: contract.address,
  // //   remaining: remaining(contract.deadline),
  // //   amount: ethereum.web3.fromWei(contract.amountPot, "ether"),
  // //   contributers: contract.participants,
    error_msg: "EtherPot is in test mode (ropsten), DO NOT send Ether from mainnet"
   }

  );
  //console.log(winner());
});

router.get('/help', function(req, res) {
  res.render('help', {layout: null});
});

router.post('/request_json', function(req, res) {
  res.send(JSON.stringify(toJSON()));
});

function winner() {
  Win.find().lean().exec(function (err, users) {
    contract.winners = JSON.stringify(users);
  });
}

function toJSON() {
  //winner();

  return {
    address: contract.address,
    amount: ethereum.web3.fromWei(contract.amountPot),
    deadline: contract.deadline,
    contributers: contract.participants,
    status: contract.status,
    winners: contract.winners
  };
}

module.exports = router;
