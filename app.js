import babel from 'babel-register';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import exphbs from 'express-handlebars';
import expressValidator from 'express-validator';
import session from 'express-session';
import flash from 'connect-flash';
import mongo from 'mongodb';
import mongoose from 'mongoose';
import index from './routes/index';
import maintenance from './routes/maintenance';
import config from './config';
import Ethereum from './ethereum';

// Init App
const app = express();

// Ethereum Contract Deployment
global.contract = {
  address: "No address available",
  deadline: null,
  status: null,
  amountPot: "0",
  contributers: [],
  winners:
  [{
          address : "99",
          amount : "0",
          timestamp : "0"
  },
  {
          address : "0x60e70d36ac0f99bc39e0dc642691b308b56105db",
          amount : "500000000000000000",
          timestamp : "20:09:04 GMT+0000 (UTC)"
  }]
};

global.ethereum = new Ethereum();
ethereum.deployContract();

// Database connect
mongoose.connect('mongodb://localhost:27017/etherpot');
let db = mongoose.connection;

db.once('open', function(){
  console.log('Connected to Database');
});

db.once('error', function(err){
  console.log(err);
});

// View Engine
var hbs = exphbs.create({
  helpers: {
    toJSON: function(object) {
      return JSON.stringify(object);
    },
  },
  defaultLayout: 'default'
});

// View Engine Settings
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

// Set Static Path
app.use(express.static(path.join(__dirname, 'public')));
app.use('/scripts', express.static(path.join(__dirname, '/node_modules')));

// Express Session
app.use(session({
  secret: 'secret',
  saveUninitialized: true,
  resave: true
}));
// Express Validator Middleware
app.use(expressValidator());
// Connect-Flash
app.use(flash());
// Global vars
app.use(function(req, res, next) {
  res.locals.title = config.title;
  next();
});
// Routes
app.use('/', index);
app.use('/maintenance', maintenance);

app.set('port', (process.env.PORT || config.port));

app.listen(app.get('port'), function() {
  console.log('Server started on port ' + app.get('port') + '...');
});

module.exports = app;
