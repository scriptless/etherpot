$(document).ready(function() {
  request();
});

setInterval(request, 1000);

var deadline = 0;

function request() {
  $.ajax({
    url: "/request_json",
    method: "POST",
    success: function(response) {
      html(response);
      console.log(response);
    },
    error: function() {
      //window.location.replace("/maintenance");
    }
  });
}

function html(response) {

  var r = JSON.parse(response);

  $('#contract_addr').val(r.address);
  $('#pot_value').html(r.amount + ' ETH');

  $("#recent_winners").html('');
  $.each(r.winners, function(idx, p){
    $("#recent_winners").append('<tr><td><i class="fa fa-circle-o"></i></td><td>' + p.address + '</td><td>' + p.amount + ' ETH</td><td>' + p.timestamp + '</td></tr>');
  });

  if(r.status != null) {
    $('#success_alert').show();
    $('#success_msg').html(r.status);
  } else {
    $('#success_alert').hide();
  }

  $('#remaining').html(remaining(r.deadline));
  $('#active_players').html('Active Pot Players (' + r.contributers.length + ')');
  $('#qr').attr('src', 'https://api.qrserver.com/v1/create-qr-code/?color=000000&bgcolor=FFFFFF&data=ethereum%3A' + r.address + '&qzone=1&margin=0&size=100x100&ecc=L');

  if(r.contributers.length > 0) {

    $("#participants").html('');
    $.each(r.contributers, function(idx, p){
      $("#participants").append('<tr><td style="color: ' +  p.color + '"><i class="fa fa-circle-o"></i></td><td>' + p.percentage + '%</td><td>' + p.address + '</td><td>' + p.amount + ' ETH</td><td>' + p.time + ' PM</td></tr>');
    });
    $('#no_parti').hide();
    $('#progresses').html('');
    $.each(r.contributers, function(idx, p){
      $("#progresses").append('<div class="progress-bar" style="background-color: ' + p.color + '; width: ' + p.percentage + '%"><span class="sr-only"></span>' + showPercentage(p.percentage) + '</div>');
    });
    $('#no_progress').hide();

  } else {

    $("#progresses").html('<div id="no_progress" class="progress-bar" style="background-color: #e9ecef; width: 100%"><span class="sr-only"></span>0%</div>');
    $('#participants').html('');
    $('#no_progress').show();
    $('#no_parti').show();

  }
}

function showPercentage(percentage) {
  if(percentage < 10) return '';
  return percentage + '%';
}

function remaining(result) {
  if(result == null) return 'There is no round at the moment :(';
  if(result == 0) return 'Start a new round by depositing Ethereum.';
  if(result * 1000 <= new Date().getTime()) return 'Pot closed. Starting new round soon.';

  var delta = Math.abs(result - (new Date().getTime() / 1000));
  var minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;
  var seconds = Math.floor(delta % 60);

  return 'Round ends in ' + minutes + " minutes and " + seconds + " seconds";
}
