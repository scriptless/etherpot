import Web3 from 'web3';
import fs from 'fs';
import solc from 'solc';
import config from './config';
import Win from './models/win';

let web3 = new Web3(
  new Web3.providers.HttpProvider(config.nodeAddress)
);

class Ethereum {

  constructor() {
    this.address = config.ownerAddress;
    this.web3 = web3;
    let output = solc.compile(fs.readFileSync(config.contractFile).toString(), 1);
    this.abi = output.contracts[':EthPot'].interface;
    this.bytecode = "0x" + output.contracts[':EthPot'].bytecode;
    this.contract = this.web3.eth.contract(JSON.parse(this.abi));
    this.unlocked = false;
    this.unlockAccount(this.address);
  }

  unlockAccount(addr) {
    console.log("Unlocking account: " + addr);
    try {
      this.web3.personal.unlockAccount(addr, config.addressPassword);
      console.log("Unlocked!");
      console.log("> Balance: " + this.web3.fromWei(this.web3.eth.getBalance(addr)) + " ETH");
      this.unlocked = true;
    } catch (error) {
      console.log("Error unlocking account: " + error);
      this.unlocked = false;
    }
  }

  deployContract() {

    if (!this.unlocked) return;

    // Check if last contract has been used or not
    if(this.contract.at('0x0513df6a9f667a9d45998e9bf926a0b1cd892b3a').started == 0) {
      console.log("Using existing contract address...");
      this.unlockAccount(ethereum.address);
      this.finishDeploy("0x0513df6a9f667a9d45998e9bf926a0b1cd892b3a");
      return;
    }

    console.log("Deploying new contract...");

    this.contract.new({
      from: this.address,
      gas: 4000000,
      data: this.bytecode
    }, function(err, c) {
      if (err) {
        console.log("Error deploying the contract: " + err);
        if (err == "Error: authentication needed: password or unlock") {
          ethereum.unlockAccount(ethereum.address);
          ethereum.deployContract();
        }
        return;
      }

      if (!c.address) {
        console.log("=============================================");
        console.log("Your contract is being deployed in transaction at http://rinkeby.etherscan.io/tx/" + c.transactionHash);
        return;
      }

      console.log("Your contract has been deployed at http://rinkeby.etherscan.io/address/" + c.address);
      console.log("=============================================");

      ethereum.finishDeploy(c.address);
    });
  }

  finishDeploy(addr) {
    contract = new Contract(this.contract.at(addr), addr);
    contract.startWatching();
  }
}

class Contract {

  constructor(instance, addr) {
    this.instance = instance;
    this.address = addr;
    this.deadline = 0;
    this.amountPot = 0;
    this.participants = [];
    this.status = null;
  }

  addDeposit(addr, amount) {
    var contributer = this.contributed(addr);

    if(contributer != null) {
      contributer.amount = web3.fromWei(+web3.toWei(contributer.amount) + +amount, "ether");
      contributer.time = new Date().toTimeString().split(' ')[0];
      return;
    }

    this.participants.push({
      address: addr,
      amount: web3.fromWei(amount, "ether"),
      percentage: Math.round(((amount / this.amountPot) * 100) * 100) / 100,
      color: this.randomColor(),
      time: new Date().toTimeString().split(' ')[0]
    });
  }

  contributed(addr) {
    for (var i = 0; i < this.participants.length; i++) {
      if(addr == this.participants[i].address) return this.participants[i];
    }
    return null;
  }

  startWatching() {
    this.instance.Deposit().watch((err, result) => {
      if (err) {
        console.log('# deposit error: ' + err);
        return;
      }
      this.amountPot = +result.args._amount + +this.amountPot;
      console.log('# deposit made: amount: ' + result.args._amount + " amountpot: " + this.amountPot);
      this.deadline = this.instance.deadline();

      this.addDeposit(result.args._addr, result.args._amount);
      this.updatePercentages();
    });

    var t = setInterval(function() {
      if (!web3.isConnected()) {
        contract.address = "No provider connection";
        console.log('# no provider trying to reconnect..');
        /*web3 = new Web3(
          new Web3.providers.HttpProvider(config.nodeAddress)
        );*/
        return;
      }

      if (contract.instance.passedDeadline() == true) {
        clearInterval(t);
        contract.address = "Finding winner...";
        contract.status = "Determining the winner...";
        var winner = contract.findWinner();

        // Save winner to Database
        new Win({address: winner, amount: contract.amountPot, timestamp: new Date().toTimeString()}).save();

        contract.status = 'Winner of ' + web3.fromWei(contract.amountPot, "ether") + ' ETH: ' + winner;
        ethereum.unlockAccount(ethereum.address);
        contract.instance.endPool(winner, {
          from: ethereum.address
        });
        contract.instance.Deposit().stopWatching();
        ethereum.deployContract();
      }

    }, 500);
  }

  randomColor() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + 1 + ')';
  }

  updatePercentages() {
    for (var i = 0; i < this.participants.length; i++) {
      var chance = Math.round(((web3.toWei(this.participants[i].amount) / this.amountPot) * 100) * 100) / 100;
      this.participants[i].percentage = chance;
    }
  }

  findWinner() {
    var amount = this.instance.amountPot();
    var array = this.instance.getParticipants();
    var players = [];

    for (var i = 0; i < array.length; i++) {
      var chance = ((this.instance.getParticipant(array[i])[0] / amount) * 100);
      players.push(chance);
      console.log('# player' + i + ' ' + this.instance.getParticipant(array[i])[0] + ' | chance: ' + chance + '%');
    }

    var perc = Math.random() * 100; // between 0 and 99.999~
    var currentPerc = 0;

    for (var pID = 0; pID < players.length; pID++) {
      if (perc < (players[pID] + currentPerc)) {
        console.log("PLAYER " + array[pID] + " HAS WON!");
        return array[pID];
      } else currentPerc += players[pID];
    }
    return null;
  }

}

module.exports = Ethereum;
module.exports.contract = Contract;
